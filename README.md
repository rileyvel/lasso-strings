# Lasso Strings

Some simple string manipulation methods to help you work with plain-text data

## How to Use

`npm i lasso-strings`

then,

`import {LassoStrings} from 'lasso-strings';` 
