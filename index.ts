/**
 *  Lasso-strings
 *  A simple collection of string utility functions to help you work with large strings.
 *
 * ----
 *
 * @author  rileyvel
 * @licence Unlicense
 *
 */

export class LassoStrings {

    /**
     * Finds the n-th match of a regex pattern in a string
     * @param str - the original string
     * @param matcher - matcher RegExp object or literal to look for
     * @param index - the index of the match to find. Starts at 0
     */
    static findNthMatch(str: string, matcher: RegExp, index: number): string[] {
        const allFound = [...str.matchAll(matcher)];

        if (allFound.length <= index) {
            throw new Error(`The match at ${index} is not found. You might have bad data.`);
        }

        return allFound[index];
    }

    /**
     * Replaces the n-th match of a regexp in a string.
     * @param str - original string
     * @param matcher - matcher RegExp object or literal to look for
     * @param index - which one to replace. Starts at 0
     * @param newValue - new string to put in the match
     */
    static replaceNthMatch(str: string, matcher: RegExp, index: number, newValue: string): string {
        let cursor = 0;
        return str.replace(matcher, (oldValue: string) => {
            if (cursor++ === index) {
                return newValue;
            } else {
                return oldValue;
            }
        });
    }

    /**
     * Losslessly split a string with the matcher at the beginning of each segment (except the first).
     * For example, ('string1string2', /\d/gi) => ['string', '1string', '2']
     */
    static losslessSplit(str: string, matcher: RegExp): string[] {

        const splitToken = this.getSplitToken(str);
        const strWithToken = str.replace(matcher, match => `${splitToken}${match}`);
        return strWithToken.split(splitToken);

    }

    private static getSplitToken(str: string): string {
        let splitToken = 'qqqq';
        while (str.includes(splitToken)) {
            splitToken += 'q';
        }
        return splitToken;
    }

}
